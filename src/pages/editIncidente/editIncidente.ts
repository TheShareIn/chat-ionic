import { Observable } from 'rxjs/Observable';
import { UsersIncidentePage } from './../usersIncidente/usersIncidente';
import { ChatsProvider } from './../../providers/chats-provider/chats-provider';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FirebaseListObservable, AngularFire } from 'angularfire2';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ChatViewPage } from '../chat-view/chat-view';

@Component({
    templateUrl: 'editIncidente.html'
})
export class EditIncidentePage  {

    //Data to update
    groupsParticipate:Observable<any[]>;
    nameIssue:String = "";
    users:any;//Only Keys
    usersToIssue = []//User Complete
    //listaUsers  = [];
    
    usersKeys  = {};//Only Keys
    
  
    
    uid:string;
    
    group:any;
    keyIssue:any;
    


    constructor(public nav: NavController, public userProvider: UserProvider,
        public chatProvider: ChatsProvider,
        public af:AngularFire, 
        public  params:NavParams){
        
        this.uid = params.data.uid;
        this.group = params.data.group;
        this.keyIssue = params.data.keyIssue;

        console.log("Information Group")
        console.log(this.group)

    }

    ngOnInit() {
        console.log("=======ngOnInit============")
        var data = this.af.database.object(`/issues/${this.keyIssue}`);
           
            data.subscribe(snapshot => {
                this.usersToIssue=[]
                this.nameIssue = snapshot.name
                console.log("Update Usersss")
                this.users = snapshot.users
                this.groupsParticipate = snapshot.users
                console.log("==================")
                this.usersKeys = this.users
                for(var u in this.users){
                    this.userProvider.getUserInfo(u).subscribe(user =>{
                        console.log("Agregando users to view in the html")
                        this.usersToIssue.push(user) 
                    });
                }    
            });
    }; 


    ngOnInitT() {
        console.log("Loading Issues");
        console.log(this.group);


        this.chatProvider.getUsersIssues(this.group.$key)
        .then(issues => {
            console.log("======GROUPS=======");
            console.log(issues);
            this.groupsParticipate = issues.map(issues => {
                console.log("KEY GROUP");
                console.log(issues);
                return issues.map(group => {
                    //FOR EACH USER GET THE INFORMATION OF THE USER
                    console.log("CONTENIDO ISSUE:::")


                    //console.log(this.af.database.object(`/issues/${group.$key}/status`))
                    //group.metadata = this.af.database.object(`/issues/${group.$key}`)
                    //console.log(group.metadata)
                    //return group;

                    group.metadata = this.af.database.object(`/issues/${group.$key}`)
                    return group;


                });
            }); 
        });


       
        
       
    };


    saveIssue(){
        this.chatProvider.updateIssue(this.nameIssue, this.usersKeys, this.uid, this.group,this.keyIssue)
        this.nav.pop();
    }

    deleteUser(key){
        for (var u in this.usersToIssue){
            if(this.usersToIssue[u].$key==key){
                this.usersToIssue.splice(parseInt(u), 1);
                delete this.usersKeys[key]
            }
        }

        var users = this.af.database.list(`/issues/${this.keyIssue}/users`);
            users.remove(key).then(_ => console.log('item deleted!'));

    }

    addUsers(){
        console.log("addUsers")
        //User Now in the issue
        console.log(this.usersKeys)
        //Users Now in the group
        console.log(this.group.users)
        var difference = {}
        for (var user in this.group.users){
            if (this.usersKeys[user] == undefined){
                console.log(this.usersKeys[user])
                difference[user] = this.group.users[user]
            }
        }
        console.log("Diffreence")
        console.log(difference) //Only Keys

        var userWithInformation = []
        for (var u in difference){
            this.userProvider.getUserInfo(u).subscribe(user =>{
                console.log("Getting information")
                userWithInformation.push(user) 
            });
        }

        console.log("Information to send")
        console.log(userWithInformation)
        console.log("Users in Issue")
        console.log(this.usersToIssue)
        

        let param = {usersNowInformation:this.usersToIssue,usersNow:this.usersKeys,differenceKeys: difference, differenceInformation:userWithInformation,uid:this.uid,keyIssue:this.keyIssue};
        console.log("Params")
        console.log(param)
        this.nav.push(UsersIncidentePage,param);
    }

    
   
}
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ChatsProvider } from '../../providers/chats-provider/chats-provider';
import { AngularFire } from 'angularfire2';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ChatViewPage }  from '../chat-view/chat-view';
import { Data } from "../../providers/Data";
import { ChatGroupViewPage } from "../chatGroup-view/chatGroup-view";

@Component({
    templateUrl: 'search.html'
})
export class SearchPage {
    chats:Observable<any[]>;
    searchTerm: string = '';
    items: any; 
    nameUser:string;
    uid:string;
    issues = {}
    listaGroupsMessages = []
    constructor(public chatsProvider: ChatsProvider, 
        public userProvider: UserProvider, 
        public af:AngularFire, 
        public nav: NavController,
        public dataService: Data
        ) {
        
            console.log("constructor")
          //this.getItems();


          this.userProvider.getUid()
        .then(uid => {
            this.uid = uid;
            this.userProvider.getUser()
            .then(userObservable => {
                userObservable.subscribe(user => {
                    this.nameUser = user["email"];
                });
            });
        });
        console.log("Constructor")
        //NEED TO CHARGE ALL CHATS / ISSUES
        this.chatsProvider.getIssuesRolled().then(gr => {
            gr.subscribe(groups=>{

                console.log("Groups")
                console.log(groups)

                groups.map(group=>{
                    console.log("Group - Key")
                    console.log(group.$key)
                    
                    var nameGroup  =   this.af.database.object(`/groups/${group.$key}/`);
                    var n = "";
                    var nameIssue = "";
                     nameGroup.subscribe(snapshot => {
                        n=snapshot.name;
                        console.log("Name Group")
                        console.log(n)

                        console.log("Group - Issues")
                        console.log(group.issues)
                        for (var key in group.issues) {
                            console.log("Issues")
                             var is =   this.af.database.object(`/issues/${key}/`);
                             is.subscribe(snapshot => {
                                console.log("Trantando de Obtener Issue Name")
                                console.log(snapshot.name)
                                nameIssue = snapshot.name

                                 this.issues[key]={
                                nameGroup:n,
                                nameIssue:nameIssue,
                            }


                             })
                            
                           
                        }
                        
                    });
                    console.log("This is the end")
                    console.log(this.issues)  
                    
                })

            });
                
            
        })
        }


    ionViewDidLoad() {
        console.log("ionViewDidLoad")
        //this.getItems();
    }
    


    getItems(){
console.log("Busqueda")
var testChats

    var  q = this.searchTerm;
        // if the value is an empty string don't filter the items
        if (!q) {
            return;
        }
        this.listaGroupsMessages = []
        console.log("Starting Search")
        console.log(this.issues)  
        console.log("Value Search")
        console.log(q)

        

        for (var key in this.issues) {
            console.log("Issues")

            var listaMessages = []
            var groupM

            var find = false
            var listMessages = this.af.database.list(`/chats/${key}/`)
                listMessages.subscribe(lista => {
                    lista.map(message =>{
                        console.log("=======MESSAGE=======")
                        console.log(message)
                        var listaChecks  = [];


                        if (message.message.indexOf(q) !== -1){
                            find = true
                            listaMessages.push( {
                                            keyMessage:message.$key,
                                            message:message.message
                                            })
                        } 

                        console.log("=======LIST FOUND=======")
                        console.log(listaMessages)


                    })
                     if (find){
                        
                        this.listaGroupsMessages.push({
                                keyIssue : key,
                                nameGroup:this.issues[key].nameGroup,
                                nameIssue:this.issues[key].nameIssue,
                                messages: listaMessages
                                
                        });

                        console.log("=======AGREGANDO ELEMENTOS LISTA GENERAL=======")
                        console.log(this.listaGroupsMessages)
                    }
                })
           
                           
        
         }

        // var issuesParticipate = []
        // this.chatsProvider.getIssuesRolled().then(groups => {
        //     console.log("************Canalizando Groups************")
        //     console.log(groups)
        //     this.chats = groups.map(group=>{
        //         console.log("************Canalizando Groups In************")
        //         console.log(group)
        //         return group.map(chat => {
        //             console.log("Imprimiendo Key Group")
        //             console.log(chat.$key)
                    
        //             if(chat.issues){
        //                  console.log("************Canalizando Groups In x2 ************")
        //                 console.log(chat.issues)

        //                 for (var key in chat.issues) {
        //                     if (chat.issues.hasOwnProperty(key)) {
        //                         //issuesParticipate.push(key);
        //                         console.log("*******URL SEARCH********")
        //                         console.log("chats"+key)
        //                         var listMessages = this.af.database.list(`/chats/${key}/`);
        //                         var nameGroup  =   this.af.database.object(`/groups/${chat.$key}/`);
        //                         var n = "";

        //                         nameGroup.subscribe(snapshot => {
        //                             n=snapshot.name;
        //                             console.log("Name Group")
        //                             console.log(n)

                                   
        //                             //---
        //                         });

        //                          //---

        //                             var listaChecks  = [];
        //                         listMessages.subscribe(items => {
        //                             items.forEach(item => {
        //                                 if(item.message == null){return;}
        //                                  console.log("*******URL SEARCH********")
        //                                 if (item.message.indexOf(q) !== -1){
        //                                     listaChecks.push({
        //                                         key:   item.nameFrom,
        //                                         value: item.message
        //                                     });
        //                                 }  
        //                             });
        //                             if(listaChecks.length != 0){
        //                                 console.log("Agregando information chat")
        //                                 chat.info = {
        //                                     'listChats':listaChecks,
        //                                     'nameGroup':n,
        //                                     'id_Group':chat.$key
        //                                 }
        //                                 console.log(chat.info)
        //                                 console.log("<>")
        //                             }
        //                             else{
        //                                 chat.info = null;
        //                             }

        //                             console.log("Loop More Interno")
        //                             console.log(chat.info)
        //                             console.log(this.chats)
        //                             console.log("=====================")
                                    
        //                         });

                                
                                
        //                          return chat;  


                                
                                
                                
        //                     }
        //                 }  
        //             }
        //         })
        //     })
        // });

    }
    getItemsA() {
        // Reset items back to all of the items
        // set q to the value of the searchbar
        var  q = this.searchTerm;
        // if the value is an empty string don't filter the items
        if (!q) {
            return;
        }
        this.chatsProvider.getChatsSearch()
          .then(chats => {
                this.chats = chats.map(chatIn => {
                    return chatIn.map(chat => {
                        var listMessages = this.af.database.list(`/chats/${chat.$key}/`);
                        var nameGroup  =   this.af.database.object(`/groups/${chat.$key}/`);
                        var n = "";
                            nameGroup.subscribe(snapshot => {
                                n=snapshot.name;
                            }
                            );
                            var listaChecks  = [];
                         listMessages.subscribe(items => {
                            // items is an array
                            items.forEach(item => {

                                if(item.message == null){
                                        return;    
                                    }
                                if (item.message.indexOf(q) !== -1){
                                     listaChecks.push(
                                        {
                                            key:   item.nameFrom,
                                            value: item.message
                                        }
                                        );
                                }
                                
                            });
                            if(listaChecks.length != 0){
                                chat.info = {'listChats':listaChecks,
                                              'nameGroup':n,
                                              'id_Group':chat.$key
                                            }
                            }
                            else{
                                chat.info = null;
                            }
                            
                        });
                        
                          return chat;

                    });
                });
            });
}
    
        
 openChat(key) {
        let arrayParts = [];
        let interlocuters = this.af.database.list(`/groups/${key}/users`);
        console.log("Iniciando Chattt");
        interlocuters.forEach(element => {
            element.forEach(e => {
                arrayParts.push (e.$value);
            });
            
        });
        
        console.log("----------------Nombre del uSUARIO Before oPEN cHAT________________");
                console.log(this.nameUser);
        let param = {uid: this.uid, nameUser: this.nameUser, keyGroup : key, interlocutors: arrayParts};
        this.nav.push(ChatGroupViewPage,param);
    

 }

}
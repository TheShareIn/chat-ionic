import { PushNotificationProvider } from './../../providers/rest-provider/pushNotification-provider';
import { Observable } from 'rxjs/Observable';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content } from 'ionic-angular';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { ChatsProvider } from '../../providers/chats-provider/chats-provider';
import { UserProvider } from '../../providers/user-provider/user-provider';
import 'rxjs/add/operator/do';

@Component({
  templateUrl: 'chatGroup-view.html',
})
export class ChatGroupViewPage {
    chts: Observable<any>;
    length: any;
    message: string;
  uid:string;
   nameUser:string;
  keyGroup:string;
  interlocutors:[string];
  chats:FirebaseListObservable<any>;  
 
  nameGroup:string;
  nameIssue:string;

  @ViewChild(Content) content: Content;
  constructor(public nav:NavController, 
  params:NavParams, 
  public chatsProvider:ChatsProvider, 
  public af:AngularFire, 
  public userProvider:UserProvider,
  public pushProvider:PushNotificationProvider
  ) {
    
    this.uid = params.data.uid;
    this.interlocutors = params.data.interlocutors;
    this.keyGroup = params.data.keyGroup;
    this.nameUser = params.data.nameUser;
    this.nameGroup = params.data.nameGroup;
    this.nameIssue = params.data.nameIssue;

        //console.log("==================")
 //console.log("Contructor")
 //console.log(this.content)
      //console.log("==================")



    console.log(":;;;;;;;;;Name del USER INTERNO::::::::::::::");
    console.log(params.data);
    // Get Chat Reference
    chatsProvider.getChatGroupRef(this.keyGroup)
    .then((chatRef:any) => {  
        //console.log("REF - CHAT");
        this.chats = this.af.database.list(chatRef);
    });


    

  }


  
  ionViewDidEnter() {
      //console.log("ionViewDidEnter")

    this.content.scrollToBottom();
  }
 ionViewWillLeave() {
     //console.log("==================")
      //console.log("ionViewWillLeave")
      //console.log(this.content)
      //console.log("==================")
    //this.content.scrollToBottom();
  }

ionViewDidLeave(){
    //console.log("==================")
 //console.log("ionViewDidLeave")
 //console.log(this.content)
      //console.log("==================")

}

ionViewDidLoad(){
    //console.log("==================")
 //console.log("ionViewDidLoad")
 //console.log(this.content)

 this.chatsProvider.content = this.content
    //console.log( this.chatsProvider.content)

       //console.log("==================")
}

  sendMessage() {
      //console.log("Mandando mensaje")

      
      if(this.message) {
          let chat = {
              from: this.uid,
              nameFrom:this.nameUser,
              message: this.message,
              type: 'message'
          };
          this.chats.push(chat).then((item) =>{
             let grp = this.af.database.object(`/issues/${this.keyGroup}/lastMessage`);
             let dateUpdate = this.af.database.object(`/issues/${this.keyGroup}/dateUpdate`);
                dateUpdate.set(new Date().toISOString());
                grp.set(item.key);

            if (this.content != null){
                this.content.scrollToBottom();
            }
          
            this.interlocutors.forEach(userK => {


                var userToken = this.af.database.object(`/users/${userK}/token`);

                userToken.subscribe(snapshot => {
                    console.log("MENSAJE A ENVIAR")
                    console.log(chat.message)
                    var header = this.nameGroup + "-" + this.nameIssue
                    this.pushProvider.postRequest(header,chat.message,snapshot.$value)
                }).unsubscribe;
                

                let lastUpdateIssueUser = this.af.database.object(`/users/${userK}/issues/${this.keyGroup}/dateUpdate`);
                    lastUpdateIssueUser.set(new Date().toISOString());
                let lastMessageUser = this.af.database.object(`/users/${userK}/issues/${this.keyGroup}/lastMessage`);
                    lastMessageUser.set(chat.message);
                let isReadMessageUser = this.af.database.object(`/users/${userK}/issues/${this.keyGroup}/isRead`);
                    isReadMessageUser.set(false);  
            });        
            //------
          } );
          this.message = "";
      }
  };
  
  sendPicture() {
      let chat = {from: this.uid, type: 'picture', picture:null};
      this.userProvider.getPicture()
      .then((image) => {
          chat.picture =  image;
          this.chats.push(chat);
      });
  }

  ionViewWillUnload(){
    //console.log("ionViewWillUnload")
     
     let isReadMessageUser = this.af.database.object(`/users/${this.uid}/issues/${this.keyGroup}/isRead`);
        isReadMessageUser.set(true);
    //this.nav.pop();
  }
}

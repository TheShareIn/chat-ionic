import { ChatsProvider } from './../../providers/chats-provider/chats-provider';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FirebaseListObservable } from 'angularfire2';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ChatViewPage } from '../chat-view/chat-view';

@Component({
    templateUrl: 'createIncidente.html'
})
export class CreateIncidentePage  {
    users:FirebaseListObservable<any[]>;
    listaChecks  = {};
    listaUsers  = [];
    usersInGroup:any; //Only Keys
    usersToIssue = []//Only Keys
    uid:string;
    nameIssue:String = "";
    group:any;
    


    constructor(public nav: NavController, public userProvider: UserProvider,
        public chatProvider: ChatsProvider,
        public  params:NavParams){

        console.log("Iniciando Creado Issue");
        this.uid = params.data.uid;
        this.group = params.data.group;
        this.usersInGroup = params.data.usersGroup;

    }

    ngOnInit() {
            this.usersInGroup.forEach(element => {
              
               console.log("User With Info");
               console.log(element.$key);
                var u = this.userProvider.getUserInfo(element.$key).subscribe(user =>
                
                {   
                        console.log("User Located");
                        console.log(user);
                        this.usersToIssue.push(user)
                });
                
                
                //this.listaUsers.pus  
            });

    };


    AddRemoveRecipeToFavorite(e:any){
        this.listaChecks[e]=!this.listaChecks[e];
        console.log(this.listaChecks);
    }
    
    createIssue(){
        console.log("Nombre de Grupooo");
        console.log(this.nameIssue);

        var userChat  = {};

        for (var k in this.listaChecks){
            var value = this.listaChecks[k];
            if (value == true){
                userChat[k]=false;
            }
        }


        userChat[this.uid]=true;


        this.chatProvider.createIssue(this.nameIssue, userChat, this.uid, this.group)
        this.nav.pop();
    }

    
   
}
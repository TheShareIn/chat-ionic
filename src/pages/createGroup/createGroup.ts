import { ChatsProvider } from './../../providers/chats-provider/chats-provider';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FirebaseListObservable } from 'angularfire2';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ChatViewPage } from '../chat-view/chat-view';

@Component({
    templateUrl: 'createGroup.html'
})
export class CreateGroupPage  {
    users:FirebaseListObservable<any[]>;
    listaChecks  = {};
    uid:string;
    nameGroup:String = "";
    


    constructor(public nav: NavController, public userProvider: UserProvider,public chatProvider: ChatsProvider) {

        console.log("Iniciando Creado Grupo");
    }

    ngOnInit() {
        this.userProvider.getUid()
        .then(uid => {
            this.uid = uid;
            this.users = this.userProvider.getAllUsers();
            
            this.users.forEach(element => {
                console.log("Print Element USER ID");
                console.log(this.uid);
                element.forEach(e=>{
                    console.log("Print Element USER");
                    console.log(e.$key);
                    if(this.uid!=e.$key){
                        this.listaChecks[e.$key] = false;
                    }
                   
                    console.log(this.listaChecks);
                });
                console.log(element);
            });



        });
    };


      AddRemoveRecipeToFavorite(e:any){
        this.listaChecks[e]=!this.listaChecks[e];
        console.log(this.listaChecks);
    }
    
    createGroup(){
        console.log("Nombre de Grupooo");
        console.log(this.nameGroup);

        var userChat  = {};

        for (var k in this.listaChecks){
            var value = this.listaChecks[k];
            if (value == true){
                userChat[k]=false;
            }
        }

        

        this.chatProvider.createGroup(this.nameGroup,userChat,this.uid)
        this.nav.pop();
    }

    
   
}
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { validateEmail } from '../../validators/email';
import { AuthProvider } from '../../providers/auth-provider/auth-provider';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { UtilProvider } from '../../providers/utils';
declare var FCMPlugin;

@Component({
	templateUrl: 'login.html'
})
export class LoginPage {
    loginForm:any;
    token:any;

    constructor(public nav:NavController,
      public auth: AuthProvider, 
      public userProvider: UserProvider,
      public util: UtilProvider,
      public storage:Storage) {

    if (typeof FCMPlugin != 'undefined') {
        FCMPlugin.getToken(
            (t) => {
                console.log("Token inside Login")
                console.log(t);
                this.token = t;
            },
            (e) => {
                console.log("Error")
                console.log(e);
            }
        );
    }


    }

    ngOnInit() {
        console.log("nNGONINIT")
        console.log(this.token)
        this.loginForm = new FormGroup({
            email: new FormControl("",[Validators.required, validateEmail]),
            password: new FormControl("",Validators.required)
        });
    }
    
	signin() {
      this.auth.signin(this.loginForm.value)
      .then((data) => {
          this.storage.set('uid', data.uid);
          this.nav.push(TabsPage);
      }, (error) => {
          console.log(error);
          if (error["code"] == "auth/wrong-password"){
            let alert = this.util.doAlert("Error","Usuario o contraseñas incorrectas","Ok");
            alert.present();
          }else if(error["code"] == "auth/user-not-found"){
            let alert = this.util.doAlert("Error","Usuario no existe","Ok");
            alert.present();
          }
          else{
            let alert = this.util.doAlert("Error","Lo sentimos, intentalo mas tarde","Ok");
            alert.present();
          }

          
          
      });
    };
    
    createAccount() {
        let credentials = this.loginForm.value;
        this.auth.createAccount(credentials)
        .then((data) => {
           this.storage.set('uid', data.uid);
           this.userProvider.createUser(credentials, data.uid,this.token);
           
        }, (error) => {
            console.log(error);
            if (error["code"] == "auth/email-already-in-use"){
                let alert = this.util.doAlert("Error","Usuario ya registrado","Ok");
                alert.present();
            }else if (error["code"] == "auth/weak-password"){
                  let alert = this.util.doAlert("Error","Contraseña demasiado corta","Ok");
                alert.present();
            }else{
                let alert = this.util.doAlert("Error","Lo sentimos, intentalo mas tarde","Ok");
                alert.present();
            }

            
         
        });
    };
}
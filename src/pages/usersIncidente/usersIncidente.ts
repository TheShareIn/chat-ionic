import { ChatsProvider } from './../../providers/chats-provider/chats-provider';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FirebaseListObservable, AngularFire } from 'angularfire2';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ChatViewPage } from '../chat-view/chat-view';

@Component({
    templateUrl: 'usersIncidente.html'
})
export class UsersIncidentePage  {

    //Data to update

    usersNow = []
    usersToIssue = []//User Complete
    usersKeys  = [];//Only Keys
    uid:string;

    listachecks=[]
    usersNowInformation=[]
    keyIssue = ""
    


    constructor(public nav: NavController, public userProvider: UserProvider,
        public chatProvider: ChatsProvider,
        public af:AngularFire, 
        public  params:NavParams,
        
    
        ){
        
            this.uid = params.data.uid;
            this.usersToIssue = params.data.differenceKeys;
            this.usersKeys = params.data.differenceInformation;
            this.keyIssue = params.data.keyIssue;
            this.usersNow = params.data.usersNow;
            this.usersNowInformation = params.data.usersNowInformation;
    }

    isInArray(value, array) {
        return array.indexOf(value) > -1;
    }

    ngOnInit() {
 
    }; 

    AddRemoveRecipeToFavorite(e:any){
        if(!this.isInArray(e,this.listachecks)){
            console.log("Agregando")
            this.listachecks.push(e)
        }else{
            var index = this.listachecks.indexOf(e)
            console.log("Elimiando")
            
            this.listachecks.splice(index,1)
        }
        
        console.log(this.listachecks);
    }


    saveUsersIssue(){
        
       // var users = this.listachecks.concat(this.usersNow);
        console.log("Users nOW")
        console.log(this.usersNow)

   
       
        //console.log(users)
        //Update List Users
        var list = {}
        this.listachecks.forEach(element => {
            list[element] = false
            //Search in the ussers to add
            console.log("Search user")
            console.log(element)
            for(var searchUser in this.usersKeys){
                var user = this.usersKeys[searchUser]
                console.log("Key to search")
                console.log(user)
                if(user.$key==element){
                    console.log("Find element!!!!")
                    this.usersNowInformation.push(user)
                }
            }
            console.log("==================")
        });

        console.log("Elements!!")
        console.log(this.usersNowInformation)
        

        console.log("List")
        console.log(list)
        for(var u in this.usersNow ){
            console.log("User")
            list[u] = this.usersNow[u]
            
        }

       

       
       
        console.log("Result")
        console.log(list)
        this.chatProvider.updateUsersIssue(list,this.keyIssue)
        this.nav.pop();
        
    }
    
    
   
}
import { ChatGroupViewPage } from './../chatGroup-view/chatGroup-view';
import { Observable } from 'rxjs/Observable';
import { ChatsProvider } from './../../providers/chats-provider/chats-provider';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FirebaseListObservable, AngularFire, FirebaseObjectObservable } from 'angularfire2';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ChatViewPage } from '../chat-view/chat-view';

@Component({
    templateUrl: 'lastChat.html'
})
export class LastChatPage {
    users:FirebaseListObservable<any[]>;
    issues:Observable<any[]>;
    userObservable:FirebaseObjectObservable<any>;
    //uid:string;

    uid:string;
    nameUser:string;

      constructor(
                public chatsProvider: ChatsProvider, 
                public userProvider: UserProvider, 
                public af:AngularFire, 
                public nav: NavController) {
                //Push
                
                //.........
      
                    
                    console.log("Imprimiendo")
                     this.chatsProvider.getIssuesUserNow()
                    .then(groups => {
                        console.log("Imprimiendo GROUPS")
                        console.log(groups)
                        
                        this.issues = groups.map(groups => {
                            console.log(groups);
                            return groups.map(group => {
                            //FOR EACH USER GET THE INFORMATION OF THE USER
                            console.log("CONTENIDO GRUPO:::")
                            console.log(group)
                            
                            //group.metadata = group

                            return group;
                            });

                            
                        }); 
                        
                    });

                    //----
                    this.userProvider.getUid()
                        .then(uid => {
                           this.uid = uid;
                           this.userProvider.getUser()
                            .then(userObservable => {
                                console.log("Information Extract User")
                                console.log(userObservable)
                                this.userObservable = userObservable;
                                this.userObservable.subscribe(user => {
                                    console.log("Information Extract User Intern")
                                    console.log(user)
                                    this.nameUser = user.email;
                                    console.log("Email User")
                                    console.log(this.nameUser)
                                });
                            });
                        });
                    
                }



    ngOnInit() {
        this.users = this.userProvider.getAllUsers()
        console.log("CONTENIDO enf:::")
    };
    
    

        ionViewDidEnter(){
        console.log("ionViewDidEnter")
            
    }

    ionViewWillEnter(){

        console.log("ionViewWillEnter")
    }

    ngOnDestroy() {
        console.log("ionViewWillEnter")
        
    }


    ngAfterViewInit() {
  // this.yourMethod
  console.log("ngAfterViewInit")
}


   
    openChat(key,keyGroup,nameGroup,nameIssue) {
        console.log("NameGroup")
        console.log(nameGroup)
        let arrayParts = [];
        let interlocuters = this.af.database.list(`/issues/${key}/users`);
        console.log("Iniciando Chattt");
        console.log(key);
        console.log(keyGroup);
        interlocuters.forEach(element => {
            element.forEach(e => {
                 console.log("*********Users in chat********");
                console.log(e);
                arrayParts.push (e.$key);

                 console.log("Users in chat");
                console.log(e.$value);

            });
            
        });
        var group
        var gr = this.af.database.list(`/issues/${keyGroup}/`).subscribe(g=>{
            group = g
        }).unsubscribe();
        let param = {uid: this.uid, 
                     nameUser: this.nameUser, 
                     keyGroup : key, 
                     interlocutors: arrayParts,
                     nameIssue:nameIssue,
                     nameGroup:nameGroup
                    };
            this.nav.push(ChatGroupViewPage,param);
    } 

 
}
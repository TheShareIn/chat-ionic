import { UsersIncidentePage } from './../pages/usersIncidente/usersIncidente';
import { EditIncidentePage } from './../pages/editIncidente/editIncidente';
import { PushNotificationProvider } from './../providers/rest-provider/pushNotification-provider';
import { CreateIncidentePage } from './../pages/createIncidente/createIncidente';
import { InsidentePage } from './../pages/incidentePage/incidentePage';
import { LastChatPage } from './../pages/lastChats/lastChat';
import { GroupsPage } from './../pages/groups/groups';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';
import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { UsersPage } from '../pages/users/users';
import { ChatsPage } from '../pages/chats/chats';
import { AccountPage } from '../pages/account/account';
import { ChatViewPage } from '../pages/chat-view/chat-view';

import { AuthProvider } from '../providers/auth-provider/auth-provider';
import { ChatsProvider } from '../providers/chats-provider/chats-provider';
import { UserProvider } from '../providers/user-provider/user-provider';
import { UtilProvider } from '../providers/utils';
import { CreateGroupPage } from "../pages/createGroup/createGroup";
import { ChatGroupViewPage } from "../pages/chatGroup-view/chatGroup-view";
import { SearchPage } from "../pages/search/search";
import { Data } from "../providers/Data";


export const firebaseConfig = {
  apiKey: "AIzaSyAzpkMEMv3xmVA8f3wYdzEz86dfEFhV9eY",
    authDomain: "chattest-2f364.firebaseapp.com",
    databaseURL: "https://chattest-2f364.firebaseio.com",
    projectId: "chattest-2f364",
    storageBucket: "chattest-2f364.appspot.com",
    messagingSenderId: "923060517739"
};

const myFirebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
}

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    LoginPage,
    UsersPage,
    ChatsPage,
    AccountPage,
    ChatViewPage,
    GroupsPage,
    CreateGroupPage,
    ChatGroupViewPage,
    SearchPage,
    LastChatPage,
    InsidentePage,
    CreateIncidentePage,
    EditIncidentePage,
    UsersIncidentePage
  ],
  imports: [
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: true,
    }),
    AngularFireModule.initializeApp(firebaseConfig, myFirebaseAuthConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    LoginPage,
    UsersPage,
    ChatsPage,
    AccountPage,
    ChatViewPage,
    GroupsPage,
    CreateGroupPage,
    ChatGroupViewPage,
    SearchPage,
    LastChatPage,
    InsidentePage,
    CreateIncidentePage,
    EditIncidentePage,
    UsersIncidentePage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler},
  AuthProvider, ChatsProvider, UserProvider, UtilProvider,PushNotificationProvider, Storage,Data]
})
export class AppModule {}

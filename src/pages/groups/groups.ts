import { CreateIncidentePage } from './../createIncidente/createIncidente';
import { InsidentePage } from './../incidentePage/incidentePage';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ChatsProvider } from '../../providers/chats-provider/chats-provider';
import { AngularFire } from 'angularfire2';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ChatViewPage } from '../chat-view/chat-view';
import { CreateGroupPage } from "../createGroup/createGroup";
import { ChatGroupViewPage } from "../chatGroup-view/chatGroup-view";
import { SearchPage } from "../search/search";

@Component({
    templateUrl: 'groups.html'
})
export class GroupsPage {
    groups:Observable<any[]>;
    //groupsParticipate:Observable<any[]>;
    viewIssuesPage = InsidentePage
    createGroupPage = CreateGroupPage;
    searchPage = SearchPage;
    uid:string;
    nameUser:string;

    constructor(public chatsProvider: ChatsProvider, 
        public userProvider: UserProvider, 
        public af:AngularFire, 
        public nav: NavController) {

        //GET THE USER ID AND THE EMAIL (<<USERNMAE>>)
        this.userProvider.getUid()
        .then(uid => {
            this.uid = uid;
            this.userProvider.getUser()
            .then(userObservable => {
                userObservable.subscribe(user => {
                    this.nameUser = user["email"];
                }).unsubscribe();
            });
        });

        //GET GROUPS TO ESPECIFIC USER
        this.chatsProvider.getGroups()
        .then(groups => {
            this.groups = groups.map(groups => {
                //console.log(groups.$key);
                return groups.map(group => {
                    //FOR EACH USER GET THE INFORMATION OF THE USER
                    //console.log("CONTENIDO GRUPO:::"+group)
                    group.metadata = this.af.database.object(`/groups/${group.$key}`)
                    
                    return group;
                });
            }); 
        });




          
        }



    

    
 openGroup(group) {
         //console.log("Open Group - Key Group");
        //console.log(group.$key);
        var ob:Observable<any> = group.metadata
        

        ob.subscribe(a=>{

            //console.log("Inside Subcribe");
            //console.log(a);
            group = a
        });
        
        let arrayParts = [];
        let interlocuters = this.af.database.list(`/groups/${group.$key}/users`).subscribe(keys => {

             //console.log("_____KEYS_____")
            //console.log(keys)

            let param = {uid: this.uid, nameUser: this.nameUser, group : group, users:keys};
           //console.log("----------------Params Open Group________________");
        //console.log(param);
        
        this.nav.push(InsidentePage,param);
        
        }
        
        );
        
    
    }
    


}
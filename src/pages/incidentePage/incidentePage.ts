import { EditIncidentePage } from './../editIncidente/editIncidente';
import { ChatGroupViewPage } from './../chatGroup-view/chatGroup-view';
import { Observable } from 'rxjs/Observable';
import { CreateIncidentePage } from './../createIncidente/createIncidente';
import { ChatsProvider } from './../../providers/chats-provider/chats-provider';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { FirebaseListObservable, AngularFire } from 'angularfire2';
import { UserProvider } from '../../providers/user-provider/user-provider';

@Component({
    templateUrl: 'incidentePage.html'
})
export class InsidentePage  {
    //users:FirebaseListObservable<any[]>;
    listaChecks  = {};
    uid:string;
    nameUser:string;
    group:any;
    interlocutors:[string];
    //groupIssues:FirebaseListObservable<any[]>;
    groupIssues:Observable<any[]>;
    constructor(
        public nav: NavController, 
        public userProvider: UserProvider,
        public chatProvider: ChatsProvider,
        public  params:NavParams,
        public af:AngularFire, 
        public chatsProvider: ChatsProvider,
        public alertCtrl: AlertController
     ) {

    this.uid = params.data.uid;
    this.interlocutors = params.data.users;
    this.group = params.data.group;
    this.nameUser = params.data.nameUser;



        console.log("Loading Issues");


       




    }

    ngOnInit() {
        console.log("Loading Issues");
        console.log(this.group);


        this.chatsProvider.getGroupIssues(this.group.$key)
        .then(issues => {
            console.log("======GROUPS=======");
            console.log(issues);
            this.groupIssues = issues.map(issues => {
                console.log("KEY GROUP");
                console.log(issues);
                return issues.map(group => {
                    //FOR EACH USER GET THE INFORMATION OF THE USER
                    console.log("CONTENIDO ISSUE:::")


                    //console.log(this.af.database.object(`/issues/${group.$key}/status`))
                    //group.metadata = this.af.database.object(`/issues/${group.$key}`)
                    //console.log(group.metadata)
                    //return group;

                    group.metadata = this.af.database.object(`/issues/${group.$key}`)
                    return group;


                });
            }); 
        });


       
        
        // this.userProvider.getUid()
        // .then(uid => {
        //     this.uid = uid;
        //     this.users = this.userProvider.getAllUsersInGroup(this.idGroup);
            
        //     this.users.forEach(element => {
        //         console.log("Print Element");
        //         console.log(this.uid);
        //         element.forEach(e=>{
        //             console.log("Print Element");
        //             console.log(e.$key);
        //             if(this.uid!=e.$key){
        //                 this.listaChecks[e.$key] = false;
        //             }
        //              console.log(this.listaChecks);
        //         });
        //         console.log(element);
        //     });



        // });
    };


    //   AddRemoveRecipeToFavorite(e:any){
    //     this.listaChecks[e]=!this.listaChecks[e];
    //     console.log(this.listaChecks);
    // }
    
    // createGroup(){
    //     console.log("Nombre de Grupooo");
    //     console.log(this.nameGroup);

    //     var userChat  = {};

    //     for (var k in this.listaChecks){
    //         var value = this.listaChecks[k];
    //         if (value == true){
    //             userChat[k]=false;
    //         }
    //     }
    //     console.log("Imprimiendo Uusuarios in the Group")
    //     console.log(userChat)
    //     this.chatProvider.createGroup(this.nameGroup,userChat,this.uid)

    //      this.nav.pop();
    // }



    openChat(key) {

    
        let arrayParts = [];
        let interlocuters = this.af.database.list(`/issues/${key}/users`);
        interlocuters.forEach(element => {
            element.forEach(e => {
                arrayParts.push (e.$key);
            });
        });
        

        var nemIsu = this.af.database.object(`/issues/${key}/name`);

        nemIsu.subscribe(snapshot => {

            var nameIssue = snapshot.$value

            console.log("Name Issue")
            console.log(nameIssue)
            let param = {uid: this.uid, nameUser: this.nameUser, keyGroup : key, interlocutors: arrayParts,nameGroup:this.group.name,nameIssue:nameIssue};
            this.nav.push(ChatGroupViewPage,param);

        }).unsubscribe;

       
    }  

    createIssuePage(){
        let param = {uid: this.uid, group : this.group,usersGroup:this.interlocutors};
        this.nav.push(CreateIncidentePage,param);
    }
    editIncidente(key){
        console.log("======Edit Incidente======")
        let param = {uid: this.uid, group : this.group,keyIssue:key};
        console.log("======Params Incidente======")
        console.log(param)
        this.nav.push(EditIncidentePage,param);

    }

    turnOff(key){
        const alert = this.alertCtrl.create({
            title: 'Borrar Issue',
            message: '¿En realidad quieres borrar el siguiente issue?',
            buttons: [
              {
                text: 'Cancelar',
                role: 'cancel',

              },
              {
                text: 'Aceptar',
                handler: () => {
                    var nemIsu = this.af.database.object(`/issues/${key}/status`);
                        nemIsu.set(false);
                }
              }
            ]
          });
          alert.present();
    }
   
}
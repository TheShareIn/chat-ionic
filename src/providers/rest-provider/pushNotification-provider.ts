import { AngularFire } from 'angularfire2';
import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions } from '@angular/http';


@Injectable()
export class PushNotificationProvider {
  


  constructor(public af: AngularFire, public http: Http) {}


  postRequest(headerMessage,bodyMessage,recipient) {
    var headers = new Headers();
    headers.append("Authorization", 'key=AAAA1uqxu2s:APA91bGAGxLdhwzpKpQq8sde3zbez48e-dIAPnkFh8dY1ItNLfkE_ceJspJba9d1SS__iRACQHloMXVFlIYk9slsXJh__9zcIRhgpxlYEjq9OId4oHT0WgpGCR5HsxRUMe-kY87j8Kxj');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
 
    let postParams = {
      //to: "d-gNlXUDois:APA91bF0cRTuMWmVZan_MUmUEEtCdQ0XmblL_N07hjTIX18xaTw4J-tQg3C34vCfyZD58xcTHv_zM4o1SFz5uG7NdSDRPsb4sl3q5X_09KYQq7x5Dhg5NecjgHtdkaHAuMcWArgyzfBc",
      to:recipient,
      notification: {
        body: bodyMessage,
        title: headerMessage,
        sound: "default"
      }
    }
    console.log("Before Log Call Service")
    console.log(postParams)
    this.http.post("https://fcm.googleapis.com/fcm/send", postParams, options)
      .subscribe(data => {
        console.log("Resultado Log Call Service")
        console.log(data);
       }, error => {
        console.log(error);// Error getting the data
      });
  }
  
}  
  

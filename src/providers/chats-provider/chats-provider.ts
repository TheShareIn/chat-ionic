import { Content } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2';
import { UserProvider } from '../user-provider/user-provider';

@Injectable()
export class ChatsProvider {

    content: Content;

    
  constructor(public af: AngularFire, public up: UserProvider) {}
  
  
  
  
  getIssuesUserNow(){
  // Get All Users of App
 
      return this.up.getUid().then(uid => {
       console.log("ID User")
       console.log(uid)
        let issues = this.af.database.list(`/users/${uid}/issues`,{
            query: {
                orderByChild: 'dateUpdate',
            }
        }).map( (arr) => { return arr.reverse(); } );
        return issues;
     });
   

  }
  // get list of Chats of a Logged In User
  getChats() {
     return this.up.getUid().then(uid => {
        let chats = this.af.database.list(`/users/${uid}/chats`);
        return chats;
     });
  }


   getChatsSearch() {
     return this.up.getUid().then(uid => {
        let chats = this.af.database.list(`/users/${uid}/chats`);
        return chats;
     });
  }

  getIssuesRolled(){

    return this.up.getUid().then(uid => {
        console.log("Imprimiendo ID")
        console.log(uid)
        let chats = this.af.database.list(`/users/${uid}/groups`);
        return chats;
     });
  }

  getLastIssues(){
     return this.af.database.list(`/issues`,{
                            query:{
                                orderByChild:'dateUpdate',
                            }
                        }).map((arr) => { return arr.reverse(); })

       
  

   
  }
  

    getGroups() {
     return this.up.getUid().then(uid => {
       // let groups = this.af.database.list(`/ownerGroup/${uid}/chats/`);
        //let groups = this.af.database.list(`/ownerGroup/${uid}/chats/`);
         let groups = this.af.database.list(`/users/${uid}/groups/`);
        return groups;
     });
  }



  getGroupIssues(key_Group){
    console.log("ID KEY GROUP")
    console.log(key_Group)
    return this.up.getUid().then(uid => {
     let groupIssues = this.af.database.list(`/users/${uid}/groups/${key_Group}/issues/`);
     return groupIssues;
    });
  }

  getUsersIssues(key_Issue){
    return this.up.getUid().then(uid => {
     let userIssues = this.af.database.list(`/issues/${key_Issue}/users`);
     return userIssues;
    });
  }


  // Add Chat References to Both users
  addChats(uid,interlocutor) {
      // First User
      let endpoint = this.af.database.object(`/users/${uid}/chats/${interlocutor}`);
      endpoint.set(true);
      
      // Second User
      let endpoint2 = this.af.database.object(`/users/${interlocutor}/chats/${uid}`);
      endpoint2.set(true);
  }


 

  getChatRef(uid, interlocutor) {
      console.log("Chat getChatRef");
      let firstRef = this.af.database.object(`/chats/${uid},${interlocutor}`, {preserveSnapshot:true});
      let promise = new Promise((resolve, reject) => {
          firstRef.subscribe(snapshot => {
                console.log("Snapshot Exists ? ");
                let a = snapshot.exists();
                   
                if(a) {
                    console.log("Snapshot Exists");
                    resolve(`/chats/${uid},${interlocutor}`);
                } else {
                    console.log("Snapshot Not Exists!!");
                    console.log("Second Ref");
                    let secondRef = this.af.database.object(`/chats/${interlocutor},${uid}`, {preserveSnapshot:true});
                    secondRef.subscribe(snapshot => {
                        console.log("Subscribe Second Ref");
                        let b = snapshot.exists();
                        if(!b) {
                            console.log("Secoond Subscribe not exist");
                            console.log(" CHAT - ADD");
                            this.addChats(uid,interlocutor);
                        }
                    });
                    console.log(" CHAT - RESOLVE 3");
                    resolve(`/chats/${interlocutor},${uid}`);
                    console.log(" CHAT - RESOLVE 4");
                }
            });
      });
      
      return promise;
  }

  getGroupChatRef(uid) {
      console.log("Chat getChatRef");
      let chats = this.af.database.object(`/users/${uid}/chats`,{preserveSnapshot:true});

      //let firstRef = this.af.database.object(`/chats/${uid},${interlocutor}`, {preserveSnapshot:true});
      let promise = new Promise((resolve, reject) => {
          chats.subscribe(snapshot => {
                console.log("GET CHATS LASTS ");
                
                   
              
            });
      });
      
      return promise;
  }



  getChatGroupRef(keyGroup) {
      console.log("==================")
      console.log("Value Content");
      console.log(this.content);
      console.log("Chat getChatRef");
      console.log("Key Group Chat "+keyGroup);
      console.log("==================")
      let firstRef = this.af.database.object(`/chats/${keyGroup}`, {preserveSnapshot:true});
      let promise = new Promise((resolve, reject) => {
          firstRef.subscribe(snapshot => {

             console.log("Value Content");
             console.log(this.content);


                console.log("Snapshot Exists ? ");
                let a = snapshot.exists();
                console.log("Valor a ::"+a );   
                resolve(`/chats/${keyGroup}`);
                if(this.content){
                    this.content.scrollToBottom();
                }
            });
      });
      
      return promise;
  }



   createGroup(name,users,uid){
    console.log("Agregando Grupos");
    let group = this.af.database.list("/groups");
    var data = {
        "name":name,
        "type":"Normal",
        "users":users,
        "lastMessage":"",
        "dateCreated":new Date().toISOString()
    }
    console.log(data);
    group.push(data).then((item) => { 
        console.log("Imprimiendo item");
        console.log(item.key);
        console.log("--------------------");

        
    let ownergroup = this.af.database.object(`/users/${uid}/groups/${item.key}/owner`);
        ownergroup.set(true);


    for (var key in users) {
         console.log("Imprimiendo each user "+key);
         console.log("--.-.--.-.-.-.-.-.--..");
        let chats = this.af.database.object(`/users/${key}/groups/${item.key}/owner`);
            chats.set(false);
    }

     let chats = this.af.database.object(`/groups/${item.key}/users/${uid}`);
            chats.set(true);

    // let chatsOwner = this.af.database.object(`/users/${uid}/groups/${item.key}`);
    //         chatsOwner.set(true);
    });
  }  

  
   createIssue(name,users,uid,idGroup){
        let issue = this.af.database.list("/issues");
        var data = {
            "name":name,
            "idGroup":idGroup.$key,
            "users":users,
            "dateCreated":new Date().toISOString(),
            "status":true
        }
        issue.push(data).then((item) => {
        let ownergroup = this.af.database.object(`/users/${uid}/groups/${idGroup.$key}/issues/${item.key}/`);
            ownergroup.set(true);

        for (var key in users) {
            let chats = this.af.database.object(`/users/${key}/groups/${idGroup.$key}/issues/${item.key}`);
                chats.set(false);
            let issueUser = this.af.database.object(`/users/${key}/issues/${item.key}/`);
            var dataIssue = {
                "lastMessage":"",
                "idGroup":item.key,
                "nameGroup":idGroup.name,
                "nameIssue":name,
                "dateUpdate":new Date().toISOString()
            } 
            issueUser.set(dataIssue)     
        }
    });
  } 
    updateIssue(name,users,uid,idGroup,idIssue){
        console.log("======updateIssue=======")
        console.log("Usuarios")
        console.log(users)
        console.log("Length Array Before Save")
        console.log(users.length)
        console.log("Usuarios End")
        let issueName = this.af.database.object(`/issues/${idIssue}/name`);
            issueName.set(name);
        let issueUsers = this.af.database.object(`/issues/${idIssue}/users`);
            issueUsers.set(users);
    }

    updateUsersIssue(users,idIssue){
       console.log("updateUsersIssue")
       console.log(users)
       console.log(idIssue)

        let issueUsers = this.af.database.object(`/issues/${idIssue}/users`);
        issueUsers.set(users);
           

    }

    deleteUsersIssue(user,idIssue){

         let issueUsers = this.af.database.list(`/issues/${idIssue}/users`);
             issueUsers.remove(user)
            
 
     }

}


import { ChatsProvider } from './../../providers/chats-provider/chats-provider';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FirebaseListObservable, AngularFire } from 'angularfire2';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ChatViewPage } from '../chat-view/chat-view';

@Component({
    templateUrl: 'users.html'
})
export class UsersPage {
    users:FirebaseListObservable<any[]>;
    uid:string;
      constructor(
                public chatsProvider: ChatsProvider, 
                public userProvider: UserProvider, 
                public af:AngularFire, 
                public nav: NavController) {

        var issues = this.af.database.list(`/issues/`,{
            query: {
                orderByChild: 'dateUpdate'
            }}).
            map( (arr) => { return arr.reverse() } ).subscribe(o => {
                console.log("TEST")
                console.log(o)



        });
                }



    ngOnInit() {
        this.userProvider.getUid()
        .then(uid => {
            this.uid = uid;
            this.users = this.userProvider.getAllUsers();
        });
    };
    
    openChat(key) {
        let param = {uid: this.uid, interlocutor: key};
        this.nav.push(ChatViewPage,param);
    }
}
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { AngularFire } from 'angularfire2';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
import { AuthProvider } from '../providers/auth-provider/auth-provider';



declare var FCMPlugin;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;
  token:any;

  constructor(platform: Platform, public af: AngularFire, public authProvider:AuthProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      StatusBar.styleDefault();
      Splashscreen.hide();
      this.intialize();
      if (typeof FCMPlugin != 'undefined') {
        FCMPlugin.getToken(
          (t) => {
            console.log("Token")
            console.log(t);
            this.token = t;
          },
          (e) => {
            console.log("Error")
            console.log(e);
          }
        );
      }
      if(typeof(FCMPlugin) != 'undefined') {
        FCMPlugin.onNotification(
          (data) => {
            console.log(data);
          },
          (e) => {
            console.log(e);
          }
        );  
      }
    });
  }

  intialize() {
    this.af.auth.subscribe(auth => {
       if(auth) {

          this.rootPage = TabsPage;
        } else {
          this.rootPage = LoginPage;
          this.rootPage.token = "ERICK"
          
        }
    });
  }
}
